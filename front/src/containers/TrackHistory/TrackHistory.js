import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {fetchTrackHistory} from "../../store/actions/track_historyActions";
import TrackHistoryListItem from "../../components/ListItems/TrackHistoryListItem";


class TracksHistory extends Component {
    componentDidMount() {
        this.props.fetchTrackHistory();
    }


    render() {
        console.log(this.props.tracks);
        return (
            <Fragment>
                <h2>
                    Your saved Tracks
                </h2>
                {this.props.tracks ? this.props.tracks.map((track, index) => {
                    return (
                        <TrackHistoryListItem
                            key={index}
                            id={track._id}
                            title={track.track.title}
                            artist={track.track.album.artist.title}
                            dateTime={track.dateTime}
                        />
                    )
                }) : null}
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.track_history.tracks,
});

const mapDispatchToProps = dispatch => ({
    fetchTrackHistory: () => dispatch(fetchTrackHistory()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TracksHistory);


