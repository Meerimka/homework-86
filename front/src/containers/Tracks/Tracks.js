import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import TrackListItem from "../../components/ListItems/TrackListItem";
import {fetchTracks} from "../../store/actions/TrackActions";
import {sendTrackHistory} from "../../store/actions/track_historyActions";


class Tracks extends Component {
    componentDidMount() {
        this.props.onFetchTracks(this.props.match.params.id);
    }



    render() {
        return (
            <Fragment>
                <h2>
                    Tracks
                </h2>
                {this.props.tracks.map(track => (
                    <TrackListItem
                        key={track._id}
                        _id={track._id}
                        title={track.title}
                        album={track.album.title}
                        duration={track.duration}
                        number={track.number}
                        sendTrack={() => this.props.sendTrackHistory(track._id)}
                    />
                ))}

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    tracks: state.tracks.tracks,
});

const mapDispatchToProps = dispatch => ({
    onFetchTracks: (id) => dispatch(fetchTracks(id)),
    sendTrackHistory: track => dispatch(sendTrackHistory(track))
});

export default connect(mapStateToProps, mapDispatchToProps)(Tracks);


