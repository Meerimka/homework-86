import React, {Component, Fragment} from 'react';
import {Button} from 'reactstrap';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import AlbumListItem from "../../components/ListItems/AlbumListItem";
import {fetchAlbums} from "../../store/actions/AlbumActions";


class Albums extends Component {
    componentDidMount() {
        this.props.onFetchAlbums(this.props.match.params.id);
    }

    render() {
        return (
            <Fragment>
                <h2>
                    Albums
                    <Link to="/albums/new">
                        <Button
                            color="primary"
                            className="float-right"
                        > Add new Album
                        </Button>
                    </Link>
                </h2>

                {this.props.albums.map(album => (
                    <AlbumListItem
                        key={album._id}
                        _id={album._id}
                        title={album.title}
                        artist={album.artist.title}
                        image={album.image}
                        production={album.production}
                    />
                ))}

            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    albums: state.albums.albums,
});

const mapDispatchToProps = dispatch => ({
    onFetchAlbums: (id) => dispatch(fetchAlbums(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Albums);


