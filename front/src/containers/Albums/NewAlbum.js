import React, {Component} from 'react';
import {connect} from "react-redux";
import {Col, Button, Form, FormGroup, Input, Label} from "reactstrap";
import {createAlbum} from "../../store/actions/AlbumActions";
import {fetchArtists} from "../../store/actions/artistActions";



class NewAlbum extends Component {
    state = {
        title: '',
        artist: '',
        production: '',
        image: null,
    };

    componentDidMount() {
        this.props.onFetchAlbums();
    }

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key])
        });
        this.props.onArtistCreated(formData);
        this.props.history.push('/');

    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <Form onSubmit={this.submitFormHandler}>
                <h2>New album</h2>
                <FormGroup row>
                    <Label sm={2} for="title">Title</Label>
                    <Col sm={10}>
                        <Input
                            type="text"
                            name="title" id="title"
                            placeholder="Enter product title"
                            value={this.state.title}
                            onChange={this.inputChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="artist">Artist</Label>
                    <Col sm={10}>
                        <Input
                            type="select" required
                            name="artist" id="artist"
                            value={this.state.artist}
                            onChange={this.inputChangeHandler}
                        >
                            <option value="">Please select artist ...</option>
                            {this.props.artists.map(artist =>{
                                return <option key={artist._id} value={artist._id}> {artist.title} </option>
                            })}
                        </Input>
                    </Col>
                </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="production">Production-year</Label>
                        <Col sm={10}>
                            <Input
                                type="number"
                                name="production" id="production"
                                value={this.state.production}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                <FormGroup row>
                    <Label sm={2} for="image">Album image</Label>
                    <Col sm={10}>
                        <Input
                            type="file"
                            name="image" id="image"
                            onChange={this.fileChangeHandler}
                        />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Col sm={{offset:2, size: 10}}>
                        <Button type="submit" color="primary">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

const mapStateToProps = state =>({
    artists: state.artists.artists,
});
const mapDispatchToProps = dispatch => ({
    onArtistCreated: albumData => dispatch(createAlbum(albumData)),
    onFetchAlbums: () => dispatch(fetchArtists())
});

export default connect(mapStateToProps, mapDispatchToProps)(NewAlbum);