import React, {Component, Fragment} from 'react';
import {Button, Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';
import {Link} from "react-router-dom";
import {connect} from "react-redux";
import ArtistListItem from "../../components/ListItems/ArtistListItem";
import {fetchArtists} from "../../store/actions/artistActions";
import ProductThumbnail from "../../components/ProductThumbnail/ProductThumbnail";
import './Artists.css';


class Artists extends Component {
    state={
        selectedItem:null
    };

    showModal = artist =>{
        this.setState({selectedItem: artist})
    };

    hideModal = () =>{
        this.setState({selectedItem: null})
    };

    componentDidMount() {
        this.props.onFetchAlbums();
    }

    render() {
        const artists= this.props.artists.map(artist => (
            <ArtistListItem
                showModal ={()=>this.showModal(artist)}
                key={artist._id}
                _id={artist._id}
                title={artist.title}
                image={artist.image}
            />
        ));
        return (
            <Fragment>
                <h2>
                    Artists
                    <Link to="/artist/new">
                        <Button
                            color="primary"
                            className="float-right"
                        > Add new Artist
                        </Button>
                    </Link>
                </h2>
                <div className="Artists">
                    {artists}
                </div>

                <Modal isOpen={!!this.state.selectedItem} toggle={this.hideModal}>
                    {this.state.selectedItem && (
                        <Fragment>
                            <ModalHeader toggle={this.hideModal}>{this.state.selectedItem.title}</ModalHeader>
                            <ModalBody>
                                <ProductThumbnail className="img-thumbnail" image={this.state.selectedItem.image}/>
                                <div>
                                    <h3>Information:</h3>
                                    &nbsp;{this.state.selectedItem.info}<br/>
                                </div>
                            </ModalBody>
                            <ModalFooter>
                                <Button color="secondary" onClick={this.hideModal}>Cancel</Button>
                            </ModalFooter>
                        </Fragment>
                    )}

                </Modal>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists,
});

const mapDispatchToProps = dispatch => ({
    onFetchAlbums: () => dispatch(fetchArtists())
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);


