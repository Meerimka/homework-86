import axios from '../../axios-api';

export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';


export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, tracks});


export const fetchTracks = (id) => {
    return dispatch => {
        return axios.get(`/tracks/?album=${id}`).then(
            response =>{
                dispatch(fetchTracksSuccess(response.data))
            }

    );
    };
};

