import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Container} from "reactstrap";
import {Switch, Route, withRouter} from "react-router-dom";
import Toolbar from "./components/UI/Toolbar/Toolbar";
import Artists from "./containers/Artists/Artists";
import NewArtist from "./containers/Artists/NewArtist";
import Albums from "./containers/Albums/Albums";
import NewAlbum from "./containers/Albums/NewAlbum";
import Tracks from "./containers/Tracks/Tracks";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import TrackHistory from "./containers/TrackHistory/TrackHistory";



class App extends Component {
  render() {
    return (
        <Fragment>
          <header>
            <Toolbar user={this.props.user}/>
          </header>
          <Container style={{marginTop: '20px'}}>
            <Switch >
                <Route  path="/" exact component={Artists}/>
                <Route path="/artist/new" exact component={NewArtist} />
                <Route  path="/albums/:id" exact component={Albums}/>
                <Route  path="/tracks/:id" exact component={Tracks}/>
                <Route path="/albums/new" exact component={NewAlbum} />
                <Route path="/register" exact component={Register}/>
                <Route path="/login" exact component={Login}/>
                <Route path="/track-history" exact component={TrackHistory}/>
            </Switch>
          </Container>
        </Fragment>
    );
  }
}

const mapStateToProps = state =>({
    user: state.users.user
});

export default withRouter(connect(mapStateToProps)(App));

