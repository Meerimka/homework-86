import React,{Fragment}  from 'react';
import {
    Navbar,
    Nav,
    NavbarBrand,
    NavItem,
    NavLink,
    UncontrolledDropdown,
    DropdownToggle,
    DropdownMenu, DropdownItem
} from "reactstrap";
import {NavLink as RouterNavLink} from 'react-router-dom';

const Toolbar = ({user}) => {
    return (
        <Navbar color="light" light expand="md">
            <NavbarBrand tag={RouterNavLink} to="/">Music</NavbarBrand>

            <Nav className="ml-auto" navbar>
                <NavItem>
                    <NavLink tag={RouterNavLink} to="/" exact>Artists</NavLink>
                </NavItem>
                {user ? (
                    <UncontrolledDropdown nav inNavbar>
                        <DropdownToggle nav caret>
                            Hello, {user.username}!
                        </DropdownToggle>
                        <DropdownMenu right>
                            <DropdownItem>
                                <NavItem>
                                    <NavLink tag={RouterNavLink} to="/track-history" exact>Your tracks</NavLink>
                                </NavItem>
                            </DropdownItem>
                            <DropdownItem divider />
                        </DropdownMenu>
                    </UncontrolledDropdown>
                ) : (
                    <Fragment>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/register" exact>Sign Up</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/login" exact>Login</NavLink>
                        </NavItem>
                    </Fragment>
                )}
            </Nav>
        </Navbar>
    );
};

export default Toolbar;