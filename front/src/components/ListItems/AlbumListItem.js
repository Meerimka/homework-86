import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";
import ProductThumbnail from "../ProductThumbnail/ProductThumbnail";
import {Link} from "react-router-dom";

const AlbumListItem = props => {
    return (
        <Fragment>
            <div  style={{'marginTop': '10px', 'display':"flex", "flexWrap": "wrap", width: "100%"}}>
        <Card style={{'marginTop': '10px', 'width':"33,3333%"}} >
            <CardBody>
                <ProductThumbnail image={props.image}/>
                <h2><span className="text-muted">Album: &nbsp;</span>
                <Link to={'/tracks/' + props._id}>
                   {props.title}
                </Link>
                </h2>
                <p>
                   <span className="text-muted" >Artist : &nbsp;</span> {props.artist}
                </p>
                <span className="text-muted">Production-year : &nbsp;</span> {props.production}
            </CardBody>
        </Card>
            </div>
        </Fragment>
    );
};

AlbumListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    artist: PropTypes.string.isRequired,
    production: PropTypes.number,
    image: PropTypes.string
};
export default AlbumListItem;