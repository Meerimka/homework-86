import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";
import ProductThumbnail from "../ProductThumbnail/ProductThumbnail";
import {Link} from "react-router-dom";
import '../../containers/Artists/Artists.css';

const ArtistListItem = props => {
    return (
        <Card className="Artist-card" style={{'marginTop': '10px', cursor: "pointer"}} onClick={()=>props.showModal(props)}>
            <CardBody>
                <ProductThumbnail image={props.image}/>
                <h2>
                <Link to={'/albums/' + props._id}>
                    {props.title}
                </Link>
                </h2>
            </CardBody>
        </Card>
    );
};

ArtistListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    image: PropTypes.string,
    showModal: PropTypes.func.isRequired

};
export default ArtistListItem;