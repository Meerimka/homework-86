import React from 'react';
import PropTypes from 'prop-types';
import {Card, CardBody} from "reactstrap";


const TrackListItem = props => {
    return (
        <Card style={{'marginTop': '10px', 'width':"33,3333%"}} onClick={props.sendTrack} >
            <CardBody>
                <h2><i className ="text-muted">Title: &nbsp;</i>
                <span>
                   {props.title}
                </span>
                </h2>
                <p>
                   <span className="text-muted" >Album : &nbsp;</span> {props.album}
                </p>
                <span className="text-muted">Duration : &nbsp;</span> {props.duration}
                <p>
                <span className="text-muted">Track-Number : &nbsp;</span> {props.number}
            </p>
            </CardBody>
        </Card>
    );
};

TrackListItem.propTypes ={
    _id: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    album: PropTypes.string.isRequired,
    duration: PropTypes.string,
    number: PropTypes.number,
    sendTrack: PropTypes.func
};
export default TrackListItem;