const mongoose = require('mongoose');

const Artist =require('./models/Artist');
const Album =require('./models/Album');
const Track =require('./models/Track');
const config = require('./config');



const run =async () => {
  await  mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for(let collection of collections){
      await collection.drop();
  }

const [artist, artist2,artist3] = await Artist.create(
    {
        title: "Eminem",
        image: "eminem.jpg",
        info: "Marshall Bruce Mathers III (born October 17, 1972), known professionally as Eminem"
    },
    {
        title: "Logic",
        image: 'logic.jpg',
        info: 'Lolkek'
    },
    {
        title: "Linkin park",
        image: 'linkin.jpg',
        info: 'Linkin Park is an American rock band from Agoura Hills, California. The band\'s current lineup comprises vocalist/rhythm guitarist Mike Shinoda, lead guitarist Brad Delson, bassist Dave Farrell, DJ/keyboardist Joe Hahn, and drummer Rob Bourdon, all of whom are founding members. Former members include bassist Kyle Christner and vocalists Mark Wakefield and Chester Bennington, the latter being a member until his passing in 2017.'
    },
);

    const  [album, album2, album3,album4] = await Album.create(
        {
            title:'Recovery',
            artist: artist._id,
            production: '2017',
            image:"recovery.jpg"

        },
        {
            title: "Boby tarantino",
            artist: artist2._id,
            production: '2016',
            image: 'logic.jpg'
        },
        {
            title: "A Thousand Suns",
            artist: artist3._id,
            production: '2010',
            image: 'thousand.jpg'
        },
    {
        title: "Living things",
            artist: artist3._id,
        production: '2012',
        image: 'livingthings.jpg'
    }
    );

    const  tracks = await Track.create(
        {
            title:'Not Afraid',
            album: album._id,
            duration: "4:18",
            number: 1

        },
        {
            title: 'Flexecution',
            album: album2._id,
            duration: '4:00',
            number: 1
        },
        {
            title: 'The Jam',
            album: album2._id,
            duration: '4:00',
            number: 2
        },
        {
            title: 'Wrist',
            album: album2._id,
            duration: '4:22',
            number: 3
        },
        {
            title: '44 Bars',
            album: album2._id,
            duration: '4:05',
            number: 4
        },
        {
            title: 'Deeper Than Money',
            album: album2._id,
            duration: '3:87',
            number: 5
        },
        {
            title: 'The Catalyst',
            album: album3._id,
            duration: '4:44',
            number: 1
        },
        {
            title: 'The Requiem',
            album: album3._id,
            duration: '2:01',
            number: 2
        },
        {
            title: 'The Radiance',
            album: album3._id,
            duration: '0:57',
            number: 3
        },
        {
            title: 'Empty Spaces',
            album: album3._id,
            duration: '5:17',
            number: 4
        },
        {
            title: 'In My Remains',
            album: album4._id,
            duration: '3:20',
            number: 1
        },
        {
            title: 'Burn It Down',
            album: album4._id,
            duration: '3:52',
            number: 2
        },
        {
            title: 'Lies Greed Misery',
            album: album4._id,
            duration: '2:27',
            number: 3
        },
    );

  return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});

