const express= require('express');
const TrackHistory = require('../models/TrackHistory');
const User = require('../models/User');
const auth = require('../middlewere/auth')

    const router = express.Router();

    router.get('/', auth, (req, res)=>{
        console.log('skjgnsojg');
        console.log(req.user);
        if(req.user){
           TrackHistory.find({user: req.user._id}).sort('-dateTime').populate({
               path: 'track',
               populate: {path: 'album', model: 'Album',
                   populate: {path: 'artist', model:  'Artist' }
               },
           })
               .then((result )=>res.send(result))
               .catch(()=>res.sendStatus(500))
        }else{
            TrackHistory.find().populate({
                path: 'track',
                populate: {path: 'album', model: 'Album',
                    populate: {path: 'artist', model:  'Artist' }
                },
            })
                .then((result )=>res.send(result))
                .catch(()=>res.sendStatus(500))
        }
    });

    router.post('/',async (req,res)=>{
        const token = req.get('Token');
        const user = await User.findOne({token});

        if(!user){
            return res.status(401).send({error:'Unauthorized'});
        }

        const track_history = new TrackHistory({
            track: req.body.trackId,
            user: user._id,
            dateTime: new Date().toISOString()
        });

         track_history.save()
             .then((result )=>res.send(result))
             .catch(()=>res.sendStatus(400))
    });

module.exports = router;