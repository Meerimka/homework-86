const express = require('express');
const Artist = require('../models/Artist');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});
const router = express.Router();

router.get('/', (req,res)=>{
    Artist.find()
        .then(artist => res.send(artist))
        .catch(()=>res.sendStatus(500));
});

router.post('/' , upload.single('image') , (req,res)=>{

    const artistData = req.body;

    console.log(artistData);

    if (req.file) {
        artistData.image = req.file.filename;
    }

    const artist = new Artist(req.body);

    artist.save()
        .then(result =>res.send(result))
        .catch(error =>res.status(400).send(error));
});


module.exports =router;
