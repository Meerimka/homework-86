const  express = require('express');
const Track = require('../models/Track');

const router = express.Router();

router.get('/', (req,res)=>{
    if(req.query.album) {
        Track.find({album: req.query.album}).populate('album').sort({number: 1}).then(result => res.send(result))
    } else {
        Track.find().sort({number: 1}).populate('album')
            .then(track => res.send(track))
            .catch(()=>res.sendStatus(500));
    }

});


module.exports =router;